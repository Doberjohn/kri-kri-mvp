import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { DataService } from '../data-service.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {  
  clients: any;
  nearbyVendors: Array<any>;
  searchQuery: string;
  radius: number;

  constructor(public db: AngularFirestore, public dataService: DataService,
    public geolocation: Geolocation, public router: Router, public platform: Platform) {

    this.searchQuery = "";
    this.nearbyVendors = [];
    this.radius = 30;

    document.querySelector('ion-tab-bar').style.display = 'none';

    db.collection('clients', ref => ref.limit(500)).valueChanges().subscribe(clients => {
      this.clients = clients;
      dataService.setClients(this.clients);
      document.querySelector('ion-tab-bar').style.display = 'flex';

      this.geolocation.getCurrentPosition().then((resp) => {
        dataService.setUserPosition(resp.coords.latitude, resp.coords.longitude);

        this.clients.forEach(client => {
          client.distance = getDistanceFromLatLonInKm(resp.coords.latitude, resp.coords.longitude, client.x, client.y);
          if (client.distance < 1000) {
            client.distanceText = client.distance.toFixed(1) + " μ.";
          } else {
            client.distanceText = (client.distance / 1000).toFixed(1) + " χλμ.";
          }
        });

        this.searchForNearby();
      }).catch((error) => {
        console.log('Error getting location', error);
      });
    });
  }

  setSelectedRadius(event) {
    this.radius = parseInt(event.detail.value);
    this.searchForNearby();
  }

  searchForNearby() {
    var userX = this.dataService.getUserPosition().x;
    var userY = this.dataService.getUserPosition().y;

    this.dataService.resetNearbyVendors();
    this.nearbyVendors = [];

    this.clients.forEach(client => {
      let distance = getDistanceFromLatLonInKm(userX, userY, client.x, client.y);      
      if (distance < this.radius) {
        this.dataService.addVendorInNearby(client);
      }
    });

    this.nearbyVendors = this.dataService.getNearbyVendors();
    this.nearbyVendors.sort((clientA, clientB) => {
      return clientA.distance - clientB.distance;
    });
  }

  navigateToClient(client) {
    this.dataService.setSelectedClient(client);
    this.router.navigateByUrl('/client-details');
  }

  submitSearch(event) {
    this.db.doc('clients/' + this.searchQuery).valueChanges().subscribe(client => {
      if (client) {
        this.navigateToClient(client);
      } else {
        this.dataService.presentAlert('Δε βρέθηκε πελάτης με αυτό το ΑΦΜ');
      }
    });
  }
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1);  // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d * 1000 // Distance in meters;
}

function deg2rad(deg) {
  return deg * (Math.PI / 180)
}
