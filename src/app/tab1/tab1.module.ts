import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LoadingSpinnerComponent } from '../loading-spinner/loading-spinner.component';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: Tab1Page }])
  ],
  providers: [Geolocation], 
  declarations: [Tab1Page, LoadingSpinnerComponent]
})
export class Tab1PageModule {}
