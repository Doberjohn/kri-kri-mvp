import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FridgeDetailsPage } from './fridge-details.page';

const routes: Routes = [
  {
    path: '',
    component: FridgeDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FridgeDetailsPage]
})
export class FridgeDetailsPageModule {}
