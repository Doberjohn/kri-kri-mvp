import { Component, OnInit } from '@angular/core';
import { DataService } from '../data-service.service';

@Component({
  selector: 'app-fridge-details',
  templateUrl: './fridge-details.page.html',
  styleUrls: ['./fridge-details.page.scss'],
})
export class FridgeDetailsPage {

  fridge: any;

  constructor(public dataService: DataService) {
    this.fridge = dataService.getSelectedFridge();
    console.log(this.fridge);
    
  }

}
