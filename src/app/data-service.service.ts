import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private clients: any[];
  private userX: number;
  private userY: number;
  private nearbyVendors: Array<any>;
  private selectedClient: any;
  private selectedFridge: any;

  constructor(public alertController: AlertController, public db: AngularFirestore) {
    this.nearbyVendors = [];
  }

  setClients(clients: any[]) {
    this.clients = clients;    
  }

  getClients() {
    return this.clients;
  }

  setUserPosition(x, y) {
    this.userX = x;
    this.userY = y;
  }

  getUserPosition() {
    return {
      x: this.userX,
      y: this.userY
    }
  }

  addVendorInNearby(vendor) {
    this.nearbyVendors.push(vendor);
  }

  resetNearbyVendors() {
    this.nearbyVendors = [];
  }

  getNearbyVendors() {
    return this.nearbyVendors;
  }

  setSelectedClient(client) {
    this.selectedClient = client;
  }

  getSelectedClient() {
    return this.selectedClient;
  }

  getClusterIcons() {
    return [
      {
        min: 3,
        max: 9,
        url: "https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m1.png"
      },
      {
        min: 10,
        max: 24,
        url: "https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m2.png"
      },
      {
        min: 25,
        max: 49,
        url: "https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m3.png"
      },
      {
        min: 50,
        max: 99,
        url: "https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m4.png"
      },
      {
        min: 100,
        url: "https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m5.png"
      }
    ]
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Σφάλμα',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  setSelectedFridge(fridge) {
    return new Promise(resolve => {
      this.db.doc('fridges/' + fridge.asset_SN).valueChanges().subscribe((fridge: any) => {
        this.selectedFridge = fridge;
        resolve(this.selectedFridge);
      });
    })
  }

  getSelectedFridge() {
    return this.selectedFridge;
  }
}
