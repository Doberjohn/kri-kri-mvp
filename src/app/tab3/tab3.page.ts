import { Component } from '@angular/core';
import { DataService } from '../data-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  clients: any;
  constructor(public dataService: DataService, public router: Router) {
    this.clients = dataService.getClients();
  }

  navigateToClient(client) {
    this.dataService.setSelectedClient(client);
    this.router.navigateByUrl('/client-details');
  }
}
