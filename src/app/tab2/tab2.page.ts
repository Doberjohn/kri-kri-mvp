import { Component, AfterViewInit } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  Marker,
  GoogleMapsEvent,
  MarkerCluster,
} from '@ionic-native/google-maps';
import { Platform } from '@ionic/angular';
import { DataService } from '../data-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements AfterViewInit {
  map: GoogleMap;
  clients: Array<any>;

  constructor(public platform: Platform, public dataService: DataService, public router: Router) { }

  async ngAfterViewInit() {
    await this.platform.ready();

    this.clients = this.dataService.getClients();
    let x = this.dataService.getUserPosition().x;
    let y = this.dataService.getUserPosition().y;

    this.loadMap(x, y);
  }

  loadMap(lat, long) {
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: lat,
          lng: long
        },
        zoom: 12,
        tilt: 30
      }
    });

    this.map.addMarkerSync({
      title: "Η τοποθεσία σας",
      position: {
        lat: lat,
        lng: long
      }
    });

    let markerCluster: MarkerCluster = this.map.addMarkerClusterSync({
      markers: this.getMarkers(),
      icons: this.dataService.getClusterIcons()
    });

    markerCluster.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
      let marker: Marker = params[1];
      
      marker.setTitle(marker.get("client").NameCus);
      marker.setSnippet(marker.get("client").street);
      marker.showInfoWindow();

      this.navigateToClient(marker.get('client'));
    });

  }

  getMarkers() {
    let markers = [];

    this.clients.forEach(client => {
      markers.push({
        position: {
          lat: client.x,
          lng: client.y
        },
        name: client.NameCus,
        address: client.street,
        client: client,
        icon: "assets/marker.png"
      });
    });

    return markers;
  }

  navigateToClient(client) {
    this.dataService.setSelectedClient(client);
    this.router.navigateByUrl('/client-details');
  }
}
