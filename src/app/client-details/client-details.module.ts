import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ClientDetailsPage } from './client-details.page';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { NFC, Ndef } from '@ionic-native/nfc/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';

const routes: Routes = [
  {
    path: '',
    component: ClientDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  providers: [BarcodeScanner, NFC, Ndef, OpenNativeSettings],
  declarations: [ClientDetailsPage]
})
export class ClientDetailsPageModule { }
