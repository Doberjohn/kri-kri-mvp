import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DataService } from '../data-service.service';
import { Platform } from '@ionic/angular';
import { GoogleMap, GoogleMaps } from '@ionic-native/google-maps';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { NFC } from '@ionic-native/nfc/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.page.html',
  styleUrls: ['./client-details.page.scss'],
})
export class ClientDetailsPage implements AfterViewInit {

  client: any;
  map: GoogleMap;
  nfcStatus: string;
  nfcSubscription: Subscription;

  constructor(public dataService: DataService, private platform: Platform, public router: Router, private openNativeSettings: OpenNativeSettings,
    private barcodeScanner: BarcodeScanner, public db: AngularFirestore, private nfc: NFC) {
    this.client = dataService.getSelectedClient();
    this.nfcStatus = 'disabled';

    setInterval(() => {
      console.log(this.nfcSubscription);
      this.nfc.enabled().then(() => {
        this.nfcStatus = 'active';
        if (!this.nfcSubscription) this.setupNFCScanner();
      }).catch(err => {
        this.nfcStatus = 'disabled';
        if (this.nfcSubscription) this.nfcSubscription.unsubscribe();
      });
    }, 2000);
  }

  async ngAfterViewInit() {
    await this.platform.ready();

    this.loadMap(this.client.x, this.client.y);
  }

  ionViewWillLeave() {
    this.nfcSubscription.unsubscribe();
  }

  loadMap(lat, long) {
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: lat,
          lng: long
        },
        zoom: 12,
        tilt: 30
      }
    });

    this.map.addMarkerSync({
      position: {
        lat: lat,
        lng: long
      },
      name: this.client.NameCus,
      address: this.client.street,
      icon: "assets/marker.png"
    });
  }

  scanSNBarcode() {
    this.barcodeScanner.scan().then(barcodeData => {
      let serialNumber = barcodeData.text;

      this.searchForFridgeInDB(serialNumber);
    }).catch(err => {
      console.log('Error', err);
      this.dataService.presentAlert('Δεν ήταν δυνατό το σκανάρισμα του σειριακού αριθμού');
    });
  }

  setupNFCScanner() {
    this.nfcSubscription = this.nfc.addNdefListener(() => {
      this.nfcStatus = 'active';
    }, (err) => {
      this.nfcStatus = 'disabled';
    }).subscribe((event) => {
      let serialNumber = this.nfc.bytesToString(event.tag.ndefMessage[0].payload).substring(3);
      this.searchForFridgeInDB(parseInt(serialNumber));
    });
  }

  searchForFridgeInDB(serialNumber) {
    console.log(serialNumber);
    
    if (!this.client.fridges) this.client.fridges = [];
    
    var index = this.client.fridges.find(fridge => {
      return fridge.asset_SN === serialNumber;
    });

    if (index) {
      this.dataService.presentAlert('Ο σειριακός αριθμός υπάρχει ήδη γι αυτόν τον πελάτη');
    } else {
      this.db.doc('fridges/' + serialNumber).valueChanges().subscribe((fridge: any) => {
        if (fridge) {
          this.client.fridges.push({
            asset_SN: fridge.asset_SN,
            AssetMaterial_descr: fridge.AssetMaterial_descr,
            AssetMaterial_id: fridge.AssetMaterial_id
          });
        } else {
          this.client.fridges.push({
            asset_SN: serialNumber,
            AssetMaterial_descr: "Άγνωστος σειριακός αριθμός",
            AssetMaterial_id: null
          });
        }
        this.db.doc('clients/' + this.client.addrcode).update({ fridges: this.client.fridges });
      });
    }
  }

  async navigateToFridge(fridge) {
    let response = await this.dataService.setSelectedFridge(fridge);
    if (response) this.router.navigateByUrl('/fridge-details');
    else this.dataService.presentAlert('Ο σειριακός αριθμός του ψυγείου δεν είναι έγκυρος');
  }

  openNFCSettings() {
    this.openNativeSettings.open('nfc_settings');
  }
}
